#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>
#include <unistd.h>

int main()
{
    int fd, check;
    char txt[100] = {0};
    mkfifo("myfifo", 0600); //umask permission for rw
    fd = open("myfifo", O_RDONLY);//we need reader first, then write  
    std::cout << "Ctrl+Z to quit\n"; 
    while(1)
    { 
        check = read(fd, txt, sizeof(txt));
        if(check>=0)std::cout << "Received: " << txt << "\n";//testing correct return value
    }
    close(fd);
}