#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>
#include <unistd.h>

int main()
{
    int fd, check;
    fd = open("myfifo", O_WRONLY);
    char txt[100] = {0};
    std::cout << "Ctrl+Z to quit\n";
    while(1)
    { 
        std::cout << "What you wanna send?\n";
        std::cin >> txt;
        check = write(fd, txt, sizeof(txt));
        if(check > 0) std::cout << "Successful sending!\n";
    }
    close(fd);
    return 0;
}